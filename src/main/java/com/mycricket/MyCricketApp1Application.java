package com.mycricket;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MyCricketApp1Application {

	public static void main(String[] args) {
		SpringApplication.run(MyCricketApp1Application.class, args);
	}

}
