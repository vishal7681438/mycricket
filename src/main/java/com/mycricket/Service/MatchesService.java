package com.mycricket.Service;

import java.util.List;
import com.mycricket.Entity.Matches;


public interface MatchesService {

	List<Matches> getAllMatches();
	
	List<Matches> getLiveMatches();
	
	List<List<String>> getPointTable();
}
