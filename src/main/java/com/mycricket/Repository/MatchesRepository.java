package com.mycricket.Repository;

import java.util.Optional;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.mycricket.Entity.Matches;

@Repository
public interface MatchesRepository extends JpaRepository<Matches,Long>{

	Optional<Matches> findByTeamHeading(String teamHeading);
}
