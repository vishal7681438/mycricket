package com.mycricket.Entity;

import java.util.Date;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name =  "crick_Matches")
public class Matches {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long matchId;
	private String teamHeading;
	private String matchNumberVanue;
	private String battingTeam;
	private String battingTeamScore;
	private String bowlTeam;
	private String bowlTeamScore;
	private String liveText;
	private String matchLink;
	private String textCompleted;
	private MatchStatus status;
	private Date date2 = new Date();;
	
	public Matches() {
		super();
		// TODO Auto-generated constructor stub
	}
	
	public Matches(String teamHeading, String matchNumberVanue, String battingTeam, String battingTeamScore,
			String bowlTeam, String bowlTeamScore, String liveText, String matchLink, String textCompleted,
			MatchStatus status, Date date) {
		super();
		this.teamHeading = teamHeading;
		this.matchNumberVanue = matchNumberVanue;
		this.battingTeam = battingTeam;
		this.battingTeamScore = battingTeamScore;
		this.bowlTeam = bowlTeam;
		this.bowlTeamScore = bowlTeamScore;
		this.liveText = liveText;
		this.matchLink = matchLink;
		this.textCompleted = textCompleted;
		this.status = status;
		this.date2 = date;
	}




	public void setMatchStatus() {
		if(textCompleted.isBlank()) { 
				this.status= MatchStatus.LIVE;
		}else {
			this.status=MatchStatus.COMPLETED;
		}
	}

	public Date getDate() {
		return date2;
	}

	public void setDate(Date date) {
		this.date2 = date;
	}

	public MatchStatus getStatus() {
		return status;
	}

	public void setStatus(MatchStatus status) {
		this.status = status;
	}

	public String getTeamHeading() {
		return teamHeading;
	}


	public void setTeamHeading(String teamHeading) {
		this.teamHeading = teamHeading;
	}


	public String getMatchNumberVanue() {
		return matchNumberVanue;
	}


	public void setMatchNumberVanue(String matchNumberVanue) {
		this.matchNumberVanue = matchNumberVanue;
	}


	public String getBattingTeam() {
		return battingTeam;
	}


	public void setBattingTeam(String battingTeam) {
		this.battingTeam = battingTeam;
	}


	public String getBattingTeamScore() {
		return battingTeamScore;
	}


	public void setBattingTeamScore(String battingTeamScore) {
		this.battingTeamScore = battingTeamScore;
	}


	public String getBowlTeam() {
		return bowlTeam;
	}


	public void setBowlTeam(String bowlTeam) {
		this.bowlTeam = bowlTeam;
	}


	public String getBowlTeamScore() {
		return bowlTeamScore;
	}


	public void setBowlTeamScore(String bowlTeamScore) {
		this.bowlTeamScore = bowlTeamScore;
	}


	public String getLiveText() {
		return liveText;
	}


	public void setLiveText(String liveText) {
		this.liveText = liveText;
	}


	public String getMatchLink() {
		return matchLink;
	}


	public void setMatchLink(String matchLink) {
		this.matchLink = matchLink;
	}


	public String getTextCompleted() {
		return textCompleted;
	}


	public void setTextCompleted(String textCompleted) {
		this.textCompleted = textCompleted;
	}


	public Long getMatchId() {
		return matchId;
	}

	public void setMatchId(Long matchId) {
		this.matchId = matchId;
	}
	
}
