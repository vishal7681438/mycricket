package com.mycricket.Controller;

import java.util.List;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.mycricket.Entity.Matches;
import com.mycricket.Service.MatchesService;


@RestController
@RequestMapping("/Matches")
public class MatchesController {
	
	private MatchesService matchesService;
	
	
	public MatchesController(MatchesService matchesService) {
		// TODO Auto-generated constructor stub
		this.matchesService= matchesService;
	}

	
	@GetMapping("/AllMatches")
	public ResponseEntity<List<Matches>> getAllMatches() {
		
		List<Matches> allMatches = matchesService.getAllMatches();
		return new ResponseEntity<List<Matches>>(allMatches,HttpStatus.OK);
		}
	
	@GetMapping("/liveMatches")
	public ResponseEntity<List<Matches>> getLiveMatches() {
		
		List<Matches> liveMatches = matchesService.getLiveMatches();
		return new ResponseEntity<>(liveMatches,HttpStatus.OK);
	}
	
	@GetMapping("/PointTable")
	public ResponseEntity<List<List<String>>> getPointTable() {
		List<List<String>> pointTable = matchesService.getPointTable();
		return new ResponseEntity<>(pointTable,HttpStatus.OK);
	}
}
